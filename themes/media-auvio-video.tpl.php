<?php

/**
 * @file
 * Media_auvio/themes/media-auvio-video.tpl.php.
 *
 * Template file for theme('media_auvio_video').
 *
 * Variables available:
 *  $uri - The media uri for the auvio video (e.g., auvio://v/123456).
 *  $video_id - The unique identifier of the auvio video (e.g., 123456).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the auvio iframe.
 *  $options - An array containing the Media auvio formatter options.
 *  $width - The width value set in Media: auvio file display options.
 *  $height - The height value set in Media: auvio file display options.
 *  $title - The Media: Auvio file's title.
 *  $alternative_content - Text to display for browsers that don't support
 *  iframes.
 */
?>
<div class="<?php print $classes; ?> media-auvio-<?php print $id; ?>">
  <iframe class="media-auvio-player" width="<?php print $width; ?>" height="<?php print $height; ?>" title="<?php print $title; ?>" src="<?php print $url; ?>" frameborder="0" allowfullscreen><?php print $alternative_content; ?></iframe>
</div>
