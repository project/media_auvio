<?php

/**
 * @file
 * Extends the MediaInternetBaseHandler class to handle Auvio videos.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetAuvioHandler extends MediaInternetBaseHandler {

  /**
   * {@inheritdoc}
   */
  public function parse($embedCode) {
    // http://www.rtbf.be/auvio/*?id=*
    $patterns = array(
      '@www\.rtbf\.be/auvio/.+\?id=(\d+)@i',
    );

    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      if (isset($matches[1])) {
        return file_stream_wrapper_uri_normalize('auvio://v/' . $matches[1]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);
    $response = drupal_http_request($this->embedCode);
    if (!isset($response->error)) {
      $doc = new DOMDocument();
      @$doc->loadHTML($response->data);
      $nodes = $doc->getElementsByTagName('title');
      if ($nodes->length > 0) {
        $file->filename = truncate_utf8($nodes->item(0)->nodeValue, 255);
      }
    }
    return $file;
  }

}
