<?php

/**
 * @file
 * Extends the MediaReadOnlyStreamWrapper class to handle Auvio videos.
 */

/**
 * MediaReadOnlyStreamWrapper implementation.
 *
 * $auvio = new MediaInternetTestStreamWrapper('auvio://v/[video-code]');
 */
class MediaAuvioStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $baseUrl = 'https://www.rtbf.be';

  /**
   * {@inheritdoc}
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return 'video/auvio';
  }

  /**
   * {@inheritdoc}
   */
  public function interpolateUrl() {
    if ($parameters = $this->get_parameters()) {
      return $this->baseUrl . '/embed/m?id=' . $parameters['v'];
    }
  }

}
